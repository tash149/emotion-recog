
from keras.optimizers import SGD
from models import simple_CNN
from utils import load_data, preprocess_input
import keras.backend as K
import tensorflow as tf
    
data_path = '../datasets/fer2013/fer2013.csv'

faces, emotions = load_data(data_path)
faces = preprocess_input(faces)
num_classes = emotions.shape[1]
image_size = faces.shape[1:]
batch_size = 128
num_epochs = 1000
    
model = simple_CNN(image_size, num_classes)
model.compile(optimizer='adam', loss='categorical_crossentropy',metrics=['accuracy'])

K.get_session().run(tf.global_variables_initializer())
model.fit(faces,emotions,batch_size,num_epochs,verbose=1,
          validation_split=.1,
          shuffle=True)
